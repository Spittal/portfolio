FROM node:alpine as build

WORKDIR /tmp
COPY ./ /tmp
RUN yarn
RUN yarn build

FROM nginx:alpine

RUN sed -i 's/80/8080/g' /etc/nginx/conf.d/default.conf

COPY --from=build /tmp/dist /usr/share/nginx/html

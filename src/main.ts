import 'virtual:uno.css'
import '@unocss/reset/normalize.css'

import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')

import { defineConfig, presetIcons } from 'unocss'
import presetUno from '@unocss/preset-uno'

export default defineConfig({
  presets: [
    presetUno(),
    presetIcons(),
  ],
  theme: {
    colors: {
      body: 'rgb(6, 4, 9)',
      primary: '#F3CCA2',
      secondary: '#f3a4a2',
      white: '#ffffff',
    },
  }
})
